import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {

    private String term;
    private final Pattern bracket;
    public final ArrayList<String> termInBracket;

    public Compiler(){
        this.term = "";
        this.bracket = Pattern.compile("(?<=\\()[^()]*(?=\\))");
        this.termInBracket = new ArrayList<String>();
    }

    public Compiler(String term){
        this.term = term.trim();
        this.bracket = Pattern.compile("(?<=\\()[^()]*(?=\\))");
        this.termInBracket = new ArrayList<String>();
    }

    public void apply(){
        extractTermInBracket(this.term);
    }

    public boolean valid(){
        return validBracket() && !this.term.isEmpty();
    }

    private boolean validBracket(){
        int bracketOpen = 0;
        int bracketClose = 0;
        for (int i = 0; i < this.term.length(); i++) {
            bracketOpen += String.valueOf(this.term.charAt(i)).equals("(") ? 1 : 0;
            bracketClose += String.valueOf(this.term.charAt(this.term.length() - (i +1))).equals(")") ? 1 : 0;
        }
        return bracketClose == bracketOpen;
    }

    private void extractTermInBracket(String term){
        if(this.valid()){
            this.termInBracket.clear();
            Matcher matcher = this.bracket.matcher(term);
            int idx = 0;
            while (matcher.find()) {
                for (int i = 0; i <= matcher.groupCount(); i++) {
                    this.termInBracket.add(matcher.group(i));
                    term = term.replaceAll("\\(" + matcher.group(i).replaceAll("\\|", "\\\\|") + "\\)", String.valueOf(idx));
                    idx++;
                }
                matcher = this.bracket.matcher(term);
            }
            this.termInBracket.add(term);
        }
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}
