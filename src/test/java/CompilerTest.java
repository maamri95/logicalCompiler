import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CompilerTest {

    @Test
    void valid_term_void() {
        Compiler compiler = new Compiler();
        assertFalse(compiler.valid());
    }

    @Test
    void valid_term_Valid_byBracket() {
        Compiler compiler = new Compiler("((a|b)&c)");
        assertTrue(compiler.valid());
    }

    @Test
    void valid_term_notValid_byBracket() {
        Compiler compiler = new Compiler("(a|b))&c");
        assertFalse(compiler.valid());
    }

    @Test
    void extract_termInBracket() {
        Compiler compiler = new Compiler("((!a|b)&c)|(d<=>c)=>c");
        ArrayList<String> array = new ArrayList<>();
        array.add("!a|b");
        array.add("0&c");
        array.add("d<=>c");
        array.add("1|2=>c");
        compiler.apply();
        assertEquals(4, compiler.termInBracket.size());
        assertArrayEquals(array.toArray(), compiler.termInBracket.toArray());
    }
}