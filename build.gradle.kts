plugins {
    java
    jacoco
}

group = "edu.kasdimerbah"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}


// Do not generate reports for individual projects
tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        xml.destination = file("${rootProject.projectDir}/build/jacoco/report.xml")
        html.isEnabled = true
        csv.isEnabled = false
    }
}
